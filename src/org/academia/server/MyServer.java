package org.academia.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class MyServer {

    private Socket clientSocket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private int portNumber = 9999;
    private StringBuilder str;
    private String bufferStr;
    private DataOutputStream dataOutputStream;

    public void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Server is connected to port: " + portNumber);
            clientSocket = serverSocket.accept();
            System.out.println("Client connected");
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        String textFile = readText(getFilePath());
        sendFile(textFile);
    }


    private String getFilePath() {

        String filePath = "resources/index.html";
        try {
            bufferStr = reader.readLine();
            filePath = bufferStr.split(" ")[1];
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        System.out.println(filePath);
        return filePath;
    }

    private String readText(String filePath) {
        File file = new File("resources" + filePath);
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return stringBuilder.toString();
    }

    private void sendFile(String textFile) {

        System.out.println("trying to send to client");
        try {
            PrintWriter outputStream = new PrintWriter(clientSocket.getOutputStream(), true);
            outputStream.println("HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: <file_byte_size> \r\n" +
                    "\r\n");
            outputStream.println(textFile);
            outputStream.close();

        } catch (IOException ioException) {

            ioException.printStackTrace();
        }
    }

    private String getType(String filePath){
        return filePath.split(".")[2];
    }

    private Byte[] readBytes(String filePath){
        File file = new File("resources" + filePath);
        byte[] buffer = new byte[1024];
       DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream()); {

        int bytesRead;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            while ((bytesRead = fileInputStream.read(buffer)) != -1) {



                }
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return
    }

    public void close() {
        try {
            reader.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

